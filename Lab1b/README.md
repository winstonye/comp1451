# **Lab 1b**
Lab 1b is the follow-up to the introductory lab in COMP1451. The project files contained in this repository will create a Person object and will test to see if the Person is valid.
## Person.java
This file contains a class that creates a Person object. The Person object has setters and getters to maintain the first name, last name, and year of birth of a person.
