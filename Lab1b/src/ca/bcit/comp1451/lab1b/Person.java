package ca.bcit.comp1451.lab1b;

/**
 * This class defines a person
 * @author Mason Liang
 * @version 1.0
 */
public class Person {
	private String firstName;
	private String lastName;
	private int	   yearOfBirth;
	double x;
	
	public static final int MIN_YEAR = 0;
	public static final int MAX_YEAR = 2018;
	
	/**
	 * @param firstName the first name of student
	 * @param lastName the last name of student
	 * @param yearOfBirth the year of birth of student
	 */
	public Person(String firstName, String lastName, int yearOfBirth) {
		setFirstName 	(firstName);
		setLastName 	(lastName);
		setYearOfBirth	(yearOfBirth);
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * @return the yearOfBirth
	 */
	public int getYearOfBirth() {
		return yearOfBirth;
	}
	
	/**
	 * @param firstName the first name to set
	 */
	private void setFirstName(String firstName) {
		if(firstName != null && !firstName.trim().isEmpty()) {
			this.firstName = firstName;
		}else {
			throw new IllegalArgumentException("invalid first name");
		}
	}
	
	/**
	 * @param lastName the last name to set
	 */
	private void setLastName(String lastName) {
		if(lastName != null && !lastName.trim().isEmpty()) {
			this.lastName = lastName;
		}else {
			throw new IllegalArgumentException("invalid last name");
		}
	}
	
	/**
	 * @param yearOfBirth the year of birth to set
	 */
	private void setYearOfBirth(int yearOfBirth) {
		if(yearOfBirth >= MIN_YEAR && yearOfBirth <= MAX_YEAR) {
			this.yearOfBirth = yearOfBirth;
		}else {
			throw new IllegalArgumentException("invalid year");
		}
	}
	
	
}
