/**
 * 
 */
package ca.bcit.comp1451.lab7a;

import java.lang.Math;

/**
 * @author A00993782
 *
 */
public abstract class iPhoneSeven extends iPhone{
	private boolean highResolutionCamera;
	private int gigabytesOfMemory;
	
	/**
	 * @param numberOfminutes
	 * @param carrier
	 * @param highResolutionCamera
	 * @param gigabytesOfMemory
	 */
	public iPhoneSeven(double numberOfminutes, String carrier, boolean highResolutionCamera, int gigabytesOfMemory) {
		super(numberOfminutes, carrier);
		setHighResolutionCamera(highResolutionCamera);
		setGigabytesOfMemory(gigabytesOfMemory);
	}

	/**
	 * @return the highResolutionCamera
	 */
	public boolean isHighResolutionCamera() {
		return highResolutionCamera;
	}

	/**
	 * @return the gigabytesOfMemory
	 */
	public int getGigabytesOfMemory() {
		return gigabytesOfMemory;
	}

	/**
	 * @param highResolutionCamera the highResolutionCamera to set
	 */
	public void setHighResolutionCamera(boolean highResolutionCamera) {
		this.highResolutionCamera = highResolutionCamera;
	}

	/**
	 * @param gigabytesOfMemory the gigabytesOfMemory to set
	 */
	public void setGigabytesOfMemory(int gigabytesOfMemory) {
		this.gigabytesOfMemory = gigabytesOfMemory;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "iPhoneSeven [highResolutionCamera=" + highResolutionCamera + ", gigabytesOfMemory=" + gigabytesOfMemory
				+ ", toString()=" + super.toString() + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + gigabytesOfMemory;
		result = prime * result + (highResolutionCamera ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof iPhoneSeven))
			return false;
		iPhoneSeven other = (iPhoneSeven) obj;
		if (gigabytesOfMemory != other.gigabytesOfMemory)
			return false;
		if (highResolutionCamera != other.highResolutionCamera)
			return false;
		if (highResolutionCamera == other.highResolutionCamera)
			return true;
		if(Math.abs(this.getNumberOfminutes() - other.getNumberOfminutes()) <= 10 && this.highResolutionCamera == other.highResolutionCamera) {
			return true;
		}
		return true;
	}

	
	
}
