/**
 * 
 */
package ca.bcit.comp1451.lab7a;

/**
 * @author A00993782
 *
 */
public abstract class iPhone extends iDevice{
	private double numberOfminutes;
	private String carrier;
	
	/**
	 * @param purpose
	 * @param numberOfminutes
	 * @param carrier
	 */
	public iPhone(double numberOfminutes, String carrier) {
		super("talking");
		setNumberOfminutes(numberOfminutes);
		setCarrier(carrier);
	}

	/**
	 * @return the numberOfminutes
	 */
	public double getNumberOfminutes() {
		return numberOfminutes;
	}

	/**
	 * @return the carrier
	 */
	public String getCarrier() {
		return carrier;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		long temp;
		temp = Double.doubleToLongBits(numberOfminutes);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof iPhone))
			return false;
		iPhone other = (iPhone) obj;
		if (Double.doubleToLongBits(numberOfminutes) != Double.doubleToLongBits(other.numberOfminutes))
			return false;
		if(Double.doubleToLongBits(numberOfminutes) - Double.doubleToLongBits(other.numberOfminutes) < 10 || 
				Double.doubleToLongBits(numberOfminutes) - Double.doubleToLongBits(other.numberOfminutes) > -10) {
			return true;
		}
		return true;
	}

	/**
	 * @param numberOfminutes the numberOfminutes to set
	 */
	public void setNumberOfminutes(double numberOfminutes) {
		this.numberOfminutes = numberOfminutes;
	}

	/**
	 * @param carrier the carrier to set
	 */
	public void setCarrier(String carrier) {
		this.carrier = carrier;
	}
	

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "iPhone [numberOfminutes= " + numberOfminutes + ", carrier= " + carrier + ", getNumberOfminutes()= "
				+ getNumberOfminutes() + ", getCarrier()= " + getCarrier() + ", purpose= " + getPurpose() + "]";
	}
	
	/**
	 * prints out instance variable details
	 */
	@Override
	public void printDetails() {
		System.out.println("Number of minutes left: " + getNumberOfminutes() + ". Carrier: " + getCarrier());
	}
	
	
}
