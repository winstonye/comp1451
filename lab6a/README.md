Lab 6 is an introductory lab to polymophism, overriding, overriding hashcodes, and the toString function. Chess pieces will be created to explain and utilize these concepts.

## ChessPiece.java ##
ChessPiece.java is an object that represents a chess piece. It has setters and getters to maintain the colour (black or white) and value (for hashcode) of the piece. The equals (==) operator is overridded and utilizes the value parameter.


## Pawn.java ##
Pawn.java is an interface that extends ChessPiece.java to represent a pawn. Pawns move forward 1 unit and has functionality to promote themselves.

## Rook.java, Queen.java, Knight.java, King.java, Bishop.java ##
All of these files are interfaces that extend ChessPiece.java to represent their corresponding piece in chess. move() will println the movement of the piece. For example, when Rook.java calls move(), 

```
horizontally or vertically
```

is printed.

## Main.java ##
This file places all the chess pieces (ex. Knights, Pawns, ..., etc.) onto an array that represents the chess board. The board is then printed to println all chess pieces that exist.
