/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class Pawn extends ChessPiece{
	boolean hasBeenPromoted;
	ChessPiece newPiece;
	
	/**
	 * @param colour white if true
	 */
	public Pawn(boolean colour) {
		super(colour, 1);
	}
	
	/**
	 * @param hasBeenPromoted the hasBeenPromoted to set
	 */
	public void setHasBeenPromoted(boolean hasBeenPromoted) {
		this.hasBeenPromoted = hasBeenPromoted;
	}
	
	/**
	 * promote pawn
	 * TODO: Go by value
	 */
	public void promote(ChessPiece newPiece) {
		if(newPiece.getValue() != 1 && newPiece.getValue() != 1000) {
			this.newPiece = newPiece;
			setHasBeenPromoted(true);
		}else {
			System.out.println("cannot be promoted");
		}
	}
	
	/**
	 * prints forward 1 for pawn
	 */
	@Override
	public void move() {
		System.out.println("forward 1");
	}
	
	/**
	 * override toString for pawn
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + getValue() + ")";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (hasBeenPromoted ? 1231 : 1237);
		result = prime * result + ((newPiece == null) ? 0 : newPiece.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		
		if (!super.equals(obj))
			return false;
		
		if (getClass() != obj.getClass())
			return false;
		
		Pawn other = (Pawn) obj;
		if (hasBeenPromoted != other.hasBeenPromoted)
			return false;
		
		if (newPiece == null) {
			if (other.newPiece != null)
				return false;
		} else if (!newPiece.equals(other.newPiece))
			return false;
		return true;
	}
}
