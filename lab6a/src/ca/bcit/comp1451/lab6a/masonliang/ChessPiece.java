/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class ChessPiece {
	private boolean white; //white if true
	private int	value;
	
	/**
	 * @param colour true = white
	 * @param importance
	 */
	public ChessPiece(boolean colour, int value) {
		this.colour = colour;
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}
	
	/**
	 * @return the colour
	 */
	public boolean isWhite() {
		return white;
	}
	
	/**
	 * prints moving
	 */
	public void move() {
		System.out.println("moving");
	}
	
	/**
	 * Overrides toString method to print colour and type of chess piece
	 */
	@Override
	public String toString() {
		if(white) {
			return "White " + getClass().getSimpleName();
		}else {
			return "Black " + getClass().getSimpleName();
		}
	}
	
	@Override
	public int hashCode() {
		return 1;
	}
	
	@Override
	public boolean equals(Object o) {
		// If object is null, return false
		if(o == null) {
			return false; 
		}
		
		// If object is compared with itself, return true
		if(o == this) {
			return true; 
		}
		
		// Typecast o to c so we can compare
		ChessPiece c = (ChessPiece)o; 
		
		
		if(this.getValue() - c.getValue() == 1 || this.getValue() - c.getValue() == -1) {
			return true;
		}else {
			return false;
		}
	}
	
	
}
