/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class Knight extends ChessPiece{

	/**
	 * @param colour white if true
	 */
	public Knight(boolean colour) {
		super(colour, 2);
	}
	
	/**
	 * knights move like a L
	 */
	@Override
	public void move() {
		System.out.println("like a L");
	}
	
	/**
	 * override toString for pawn
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + getValue() + ")";
	}
}
