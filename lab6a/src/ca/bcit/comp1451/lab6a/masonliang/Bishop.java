/**
 * 
 */
package ca.bcit.comp1451.lab6a.masonliang;

/**
 * @author liang
 *
 */
public class Bishop extends ChessPiece{

	/**
	 * @param colour white if true
	 */
	public Bishop(boolean colour) {
		super(colour, 3);
	}
	
	/**
	 * bishops moves diagonally
	 */
	@Override
	public void move() {
		System.out.println("diagonally");
	}
	
	/**
	 * override toString for bishop
	 */
	@Override
	public String toString() {
		return super.toString() + "(" + getValue() + ")";
	}
}
