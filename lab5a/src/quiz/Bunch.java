package quiz;

import java.util.ArrayList;

public class Bunch {
	private ArrayList<W> bunch;
	
	public Bunch() {
		bunch = new ArrayList<>();
		
		W w1 = new W(); // creates a new W called w1
		bunch.add(w1);  // adds w1 into arraylist
		W w2 = bunch.get(0); // creates w2, which is w1 from arraylist
		w2.w();				 // runs w() from w2
		
		X x1 = new X(); // creates a new X called x1;
		bunch.add(x1);  // adds x1 to arraylist (because X is a W?)
		X x2 = (X)bunch.get(1); //have to cast because arraylist only accepts W
		x2.x();
		
		W w3 = new X();
		bunch.add(w3);
		W w4 = bunch.get(2);
		w4.w();
		
		X x3 = new X(); //see Car c2 = new Vehicle();
		bunch.add(x3);
		X x4 = (X)bunch.get(3);
		x4.x();
	}
}
