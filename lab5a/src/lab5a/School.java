/**
 * 
 */
package lab5a;

import java.util.HashMap;
import java.util.Map;

/**
 * @author liang
 *
 */
public class School{
	private Person president;
	private HashMap<String, Student> students;
	
	/**
	 * @param firstName
	 * @param lastName
	 * @param yearOfBirth
	 * @param gender
	 * @param studentNumber
	 * @param BCITStudentNumber
	 * @param major
	 * @param campus
	 * @param president
	 * @param students
	 */
	public School() {
		
		president = new Person("Kathy", "Kinloch", 1960, "female");
		
		students = new HashMap<>();
		
		Student S1 = new Student("Mark", "Zukerberg", 1984, "male", 12345678, "computer science");
		Student S2 = new Student("Michelle", "Obama", 1964, "female", 98765432, "law");
		
		BCITStudent B1 = new BCITStudent("Markus", "Frind", 1979, "male", 12345678, "computer science", "Downtown");
		BCITStudent B2 = new BCITStudent("Glora", "Marcarenko", 1969, "female", 88877777, "journalism", "Burnaby");
		
		students.put(String.valueOf(S1.getStudentNumber()), S1);
		students.put(String.valueOf(S2.getStudentNumber()), S2);
		
		students.put(B1.getBCITStudentNumber(), B1);
		students.put(B2.getBCITStudentNumber(), B2);
		
		for(Map.Entry<String, Student> s : students.entrySet()) {
			s.getValue().printDetails();
		}
		
		president.printDetails();
	}
	
	
	
	
	
}
