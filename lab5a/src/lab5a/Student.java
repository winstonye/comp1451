/**
 * 
 */
package lab5a;

/**
 * @author A00993782
 *
 */
public class Student extends Person{
	private int studentNumber;
	private String major;
	/**
	 * @param firstName
	 * @param lastName
	 * @param yearOfBirth
	 * @param gender
	 * @param studentNumber
	 * @param major
	 */
	public Student(String firstName, String lastName, int yearOfBirth, String gender, int studentNumber,
			String major) {
		super(firstName, lastName, yearOfBirth, gender);
		setStudentNumber (studentNumber);
		setMajor (major);
	}
	
	public void printDetails() {
		if(getGender().equalsIgnoreCase("male")) {
			System.out.println(getFirstName() + " " + getLastName() + " is a " + getClass().getSimpleName() + " of " + getMajor() + "(st# " + getStudentNumber() + "). " + " He was born in " + getYearOfBirth() + ".");
		}else {
			System.out.println(getFirstName() + " " + getLastName() + " is a " + getClass().getSimpleName() + " of " + getMajor() + "(st# " + getStudentNumber() + "). " + " She was born in " + getYearOfBirth() + ".");
		}
	}

	/**
	 * @return the studentNumber
	 */
	public int getStudentNumber() {
		return studentNumber;
	}

	/**
	 * @return the major
	 */
	public String getMajor() {
		return major;
	}

	/**
	 * @param studentNumber the studentNumber to set
	 */
	public void setStudentNumber(int studentNumber) {
		this.studentNumber = studentNumber;
	}

	/**
	 * @param major the major to set
	 */
	public void setMajor(String major) {
		this.major = major;
	}
}
