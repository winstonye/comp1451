/**
 * 
 */
package lab5a;

/**
 * This class defines a person
 * @author Mason Liang
 *
 */
public class Person {
	private String firstName;
	private String lastName;
	private int	   yearOfBirth;
	private String gender;
	
	/**
	 * @param firstName first name of person	
	 * @param lastName last name of person
	 * @param yearOfBirth year when person is born
	 * @param gender male or female
	 */
	public Person(String firstName, String lastName, int yearOfBirth, String gender) {
		setFirstName (firstName);
		setLastName (lastName);
		setYearOfBirth (yearOfBirth);
		setGender (gender);
	
	}

	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * @return the yearOfBirth
	 */
	public int getYearOfBirth() {
		return yearOfBirth;
	}

	/**
	 * @return the gender of human
	 */
	public String getGender() {
		return gender;
	}

	/** 
	 * @param firstName the first name to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * @param lastName the last name to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * @param yearOfBirth the year of birth to set
	 */
	public void setYearOfBirth(int yearOfBirth) {
		this.yearOfBirth = yearOfBirth;
	}

	/**
	 * @param gender the gender of person to set, has to be male or female
	 */
	public void setGender(String gender) {
		if(gender.equalsIgnoreCase("male") || gender.equalsIgnoreCase("female")) {
			this.gender = gender;
		}else {
			throw new IllegalArgumentException("invalid gender");
		}
	}
	
	/**
	 * Prints the details of the person
	 */
	public void printDetails() {
		if(getGender().equalsIgnoreCase("male")) {
			System.out.println(getFirstName() + " " + getLastName() + " is a " + getClass().getSimpleName() + ". " + "He was born in " + getYearOfBirth() + ".");
		}else {
			System.out.println(getFirstName() + " " + getLastName() + " is a " + getClass().getSimpleName() + ". " + "She was born in " + getYearOfBirth() + ".");
		}
	}
}
