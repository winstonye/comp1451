# **Lab 5a**
Lab 5a is a cumulative lab that covers all material from Lab 1-4. A school database is created with a School object that holds BCITStudent and Student objects.
## Person.java ##
Person.java is a class for a person. This class has setters and getters that maintains the first name, last name, year of birth, and gender of the Person object.
## Student.java ##
Student.java is an interface for a student and it extends the Person object. This interface has setters and getters for a student number and major.
## BCITStudent.java ##
BCITStudent.java is an interface for a BCIT student and it extends the Student object. This interface has setters and getters to maintain the campus the BCIT student attends. There are 4 campuses that can be picked: Burnaby, Downtown, Richmond, and North Vancouver.
## School.java ##
School.java creates a hash map that maps a student number and student pairing.