package ca.bcit.comp1451.jasonharrison;

public class Main {
	public static void main(String[] args) {
		try {
			Car c = new Car("dodge", "viper", "red");
			c.setColor("blue");
		}catch(IllegalCarException e) {
			System.out.println("something wrong");
		}catch(Exception e) {
			System.out.println(e.getMessage());
			
		}finally {
			System.out.println("the end");
		}
		
		
	}
}
