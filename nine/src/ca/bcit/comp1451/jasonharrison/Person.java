package ca.bcit.comp1451.jasonharrison;

public class Person {
	private int yearBorn;
	private int weightKg;

	/**
	 * 
	 * @param yearBorn
	 * @throws IllegalYearBornException if year is 0
	 */
	public Person(int yearBorn) throws 
		IllegalYearBornException{
		if(yearBorn == 0) {
			throw new IllegalYearBornException(
					"year born cannot be zero");
		}
		
		this.yearBorn = yearBorn;
	}
	
	/**
	 * 
	 * @param kg
	 * @throws IllegalArgumentException
	 */
	public void setWeightKg(int kg) throws
		IllegalArgumentException{
		if(kg <= 0) {
			throw new IllegalArgumentException(
					"cannot possibly weigh " + kg + "kg");
		}
		weightKg = kg;
	}
}
