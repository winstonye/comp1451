package ca.bcit.comp1451.jasonharrison;

public class IllegalCarException extends Exception{
	
	public IllegalCarException(String m) {
		super(m);
	}
}
