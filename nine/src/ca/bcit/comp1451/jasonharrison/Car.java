package ca.bcit.comp1451.jasonharrison;

public class Car {
	
	private String make;
	private String model;
	private String color;
	
	/**
	 * 
	 * @param make
	 * @param model
	 * @throws IllegalCarException
	 */
	public Car(String make, String model, 
			String color) throws IllegalCarException{
		if(make == null || model == null) {
			throw new IllegalCarException(
					"make/model cannot be null");
		}
		
		this.make  = make;
		this.model = model;
		setColor(color);
	}
	
	/**
	 * 
	 * @param color
	 * @throws IllegalCarException
	 */
	public void setColor(String color) throws
		IllegalCarException{
		if(color == null) {
			throw new IllegalCarException(
				"color cannot be null");
		}
		this.color = color;
	}
	
}
