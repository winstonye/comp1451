/**
 * 
 */
package ca.bcit.comp1451.lab1a;

/**
 * This class defines a day.
 * @author Mason Liang
 * @version 1.0
 */
public class Date {
	private int day;
	private int month;
	private int year;
	
	public static final int MIN_DAY = 1;
	public static final int MAX_DAY = 31;
	public static final int MIN_MONTH = 1;
	public static final int MAX_MONTH = 12;
	public static final int MIN_YEAR = 0;
	public static final int MAX_YEAR = 9999;
	public static final int YEAR_UNDER_1000 = 1000;
	public static final int YEAR_UNDER_100 = 100;
	public static final int UNDER_10 = 10;
	
	public Date(int year, int month, int day) {
		setDay (day);
		setMonth (month);
		setYear (year);
	}

	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		if(day <= MAX_DAY && day >= MIN_DAY) {
			this.day = day;
		}else {
			throw new IllegalArgumentException("invalid day");
		}
	}

	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
		
	}

	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		if(month <= MAX_MONTH && month >= MIN_MONTH) {
			this.month = month;
		}else {
			throw new IllegalArgumentException("invalid month");
		}
	}

	/**
	 * @return the day
	 */
	public int getYear() {
		return year;
	}

	/**
	 * @param day the day to set
	 */
	public void setYear(int year) {
		if(year <= MAX_YEAR && year > MIN_YEAR) {
			this.year = year;
		}else {
			throw new IllegalArgumentException("invalid year");
		}
	}
	
	/**
	 * 
	 * @return format of date in YYYY-MM-DD
	 */
	public String getDate() {
		if(getYear() < YEAR_UNDER_1000 && getMonth() < UNDER_10 && getDay() < UNDER_10) {
			return String.format("0" + getYear() + "-0" + getMonth() + "-0" + getDay());
		}else if(getYear() < YEAR_UNDER_100 && getMonth() < UNDER_10 && getDay() < UNDER_10) {
			return String.format("00" + getYear() + "-0" + getMonth() + "-0" + getDay());
		}else if(getYear() < UNDER_10 && getMonth() < UNDER_10 && getDay() < UNDER_10) {
			return String.format("000" + getYear() + "-0" + getMonth() + "-0" + getDay());
		}else{
			return String.format(getYear() + "-" + getMonth() + "-" + getDay());
		}
	}
}
