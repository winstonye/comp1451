/**
 * 
 */
package ca.bcit.comp1451.lab9a.masonliang;

/**
 * @author liang
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		BookStore bs = new BookStore();
		bs.addBooks();
		bs.displayBooks();

	}

}
