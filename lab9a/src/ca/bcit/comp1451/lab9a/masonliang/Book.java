/**
 * 
 */
package ca.bcit.comp1451.lab9a.masonliang;

/**
 * This is a book class
 * @author liang
 *
 */
public class Book implements Comparable<Book>{
	private Name firstName;
	private Name lastName;
	protected String title;
	private int yearPublished;
	
	/**
	 * Creates a book
	 * @param firstName
	 * @param lastName
	 * @param title
	 * @param yearPublished
	 */
	public Book(Name firstName, Name lastName, String title, int yearPublished) {
		try {
			setFirstName(firstName);
			setLastName(lastName);
			setTitle(title);
		}catch(IllegalArgumentException e) {
			System.out.println("Check null or empty: " + e.getClass() + " was " + e.getMessage());
		}
		
		try {
			setYearPublished(yearPublished);
		}catch(IllegalBookDateException e){
			System.out.println("Year is " + e.getMessage() + ", must be before 2016");
		}
		
	}

	/**
	 * @return the firstName
	 */
	public final Name getFirstName() {
		return firstName;
	}

	/**
	 * @param firstName the firstName to set
	 */
	public final void setFirstName(Name firstName) throws IllegalArgumentException{
		if(firstName == null) {
			throw new IllegalArgumentException("first name cannot be null");
		}
		if(firstName.getFirstName().isEmpty()) {
			throw new IllegalArgumentException("first name cannot be empty");
		}
		this.firstName = firstName;
	}

	/**
	 * @return the lastName
	 */
	public final Name getLastName() {
		return lastName;
	}

	/**
	 * @param lastName the lastName to set
	 */
	public final void setLastName(Name lastName) throws IllegalArgumentException{
		if(lastName == null) {
			throw new IllegalArgumentException("last name cannot be null");
		}
		if(lastName.getFirstName().isEmpty()) {
			throw new IllegalArgumentException("last name cannot be empty");
		}
		this.lastName = lastName;
	}

	/**
	 * @return the title
	 */
	public final String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public final void setTitle(String title) throws IllegalArgumentException{
		if(title == null) {
			throw new IllegalArgumentException("title cannot be null");
		}
		if(title.isEmpty()) {
			throw new IllegalArgumentException("title cannot be empty");
		}
		this.title = title;
	}

	/**
	 * @return the yearPublished
	 */
	public final int getYearPublished() {
		return yearPublished;
	}

	/**
	 * @param yearPublished the yearPublished to set
	 */
	public final void setYearPublished(int yearPublished) throws IllegalBookDateException{
		if(yearPublished > 2016) {
			throw new IllegalBookDateException("year must be before 2016");
		}
		this.yearPublished = yearPublished;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Book [firstName=" + firstName + ", lastName=" + lastName + ", title=" + title + ", yearPublished="
				+ yearPublished + "]";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + yearPublished;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Book))
			return false;
		Book other = (Book) obj;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (yearPublished != other.yearPublished)
			return false;
		return true;
	}
	
	/**
	 * Compare to title abc
	 */
	public int compareTo(Book b) {
		return (int)(this.getTitle().compareTo(b.getTitle()));
	}
}
