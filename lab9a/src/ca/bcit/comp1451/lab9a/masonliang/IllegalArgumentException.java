/**
 * 
 */
package ca.bcit.comp1451.lab9a.masonliang;

/**
 * @author liang
 *
 */
public class IllegalArgumentException extends Exception{
	
	public IllegalArgumentException(String m) {
		super(m);
	}
}
