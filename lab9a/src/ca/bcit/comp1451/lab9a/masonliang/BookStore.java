/**
 * 
 */
package ca.bcit.comp1451.lab9a.masonliang;

import java.util.ArrayList;
import java.util.Collections;

/**
 * This class defines a bookstore
 * @author liang
 *
 */
public class BookStore {
	private ArrayList<Book> books = new ArrayList<>();
	
	/**
	 * Add books to arraylist
	 */
	public void addBooks() {
		try {
			Book b = new Book(new Name("Min Jin"), new Name("Lee"), "Pachinko", 2015);
			b.setFirstName(new Name("Min Jin"));
			b.setLastName(new Name("Lee"));
			b.setTitle("Pachinko");
			b.setYearPublished(2014);
			books.add(b);
		}catch(IllegalBookDateException e) {
			System.out.println("Year is " + e.getMessage() + ", must be before 2016");
		}catch(IllegalArgumentException e) {
			System.out.println("Check name or title");
		}
	}
	
	/**
	 * display books before and after sorting
	 */
	public void displayBooks() {
		for(Book book: books) {
			System.out.println(book.getTitle() + " by " + book.getFirstName().getFirstName() + " " + book.getLastName().getFirstName() + " was published on " + book.getYearPublished());
		}
		Collections.sort(books);
		for(Book book: books) {
			System.out.println(book.getTitle() + " by " + book.getFirstName().getFirstName() + " " + book.getLastName().getFirstName() + " was published on " + book.getYearPublished());
		}
	}
}
