package ca.bcit.comp1451.lesson2.jasonharrison;

import java.util.Scanner;
import java.util.Random;

public class GuessingGame {
	public static void main(String[] args) {
	
		boolean keepPlaying = true;
		Scanner s = new Scanner(System.in);
		Random  r = new Random();
		int     computerNumber = -1;
		int     playerGuess    = -1;
		
		while(keepPlaying) {
			computerNumber = r.nextInt(10) + 1;
			System.out.print("enter a number 1 - 10: ");
			
			while(s.hasNext()) {
				if(s.hasNextInt()){
					playerGuess = s.nextInt();
					
					if(playerGuess > 10) {
						System.out.println("out of bounds; too high: " + playerGuess);
					}else if(playerGuess < 1) {
						System.out.println("out of bounds; too low: " + playerGuess);
					}else{ // the user entered 1 - 10
						if(playerGuess == computerNumber){
							System.out.println("correct. you got it: " + computerNumber);
							System.out.println("play again? Y/N");
							if(s.hasNext()) {
								String input = s.next();
								if(input.equalsIgnoreCase("y")) {
									keepPlaying = true;
								}else{
									keepPlaying = false;
								}
								break; // kill the inner loop
							}
						}else if(playerGuess > computerNumber) {
							System.out.println("too high: ("+computerNumber+")");
						}else{
							System.out.println("too low: ("+computerNumber+")");
						}
					}
				}else{
					System.out.println("error input: " + s.next());
				}
				System.out.print("enter another number 1 - 10: ");
			}
		}
		System.out.println("the end");
		
	}
}
