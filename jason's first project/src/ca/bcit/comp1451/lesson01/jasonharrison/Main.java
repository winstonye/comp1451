package ca.bcit.comp1451.lesson01.jasonharrison;

public class Main {
	public static void main(String[] args) {
		BookStore b = new BookStore();
		b.displayAllTitlesInUpperCase();
		
		Library l = new Library();
		l.displayAllTitlesInUpperCase();
		
		for(String s : args) {
			System.out.println(s);
		}
		
		b.whatever = 77;
		System.out.println(b.whatever);
	}
}
