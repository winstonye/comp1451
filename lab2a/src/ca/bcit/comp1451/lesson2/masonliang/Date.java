package ca.bcit.comp1451.lesson2.masonliang;

/**
 * This class defines a date
 * @author Mason Liang
 * @version 1.0
 */
public class Date {
	private int day;
	private int month;
	private int year;
	
	public static final int MIN_DAY 			= 1;
	public static final int MAX_DAY				= 31;
	public static final int MIN_MONTH 			= 1;
	public static final int MAX_MONTH 			= 12;
	public static final int MIN_YEAR 			= 1600;
	public static final int MAX_YEAR 			= 2200;
	public static final int CURRENT_YEAR 		= 2018;
	public static final int LAST_TWO_DIGITS 	= 100;
	
	private static final int SATURDAY 			= 0;
	private static final int SUNDAY 			= 1;
	private static final int MONDAY 			= 2;
	private static final int TUESDAY 			= 3;
	private static final int WEDNESDAY 			= 4;
	private static final int THURSDAY 			= 5;
	private static final int FRIDAY 			= 6;
	
	public static final int JANUARY 			= 1;
	public static final int FEBRUARY 			= 2;
	public static final int MARCH 				= 3;
	public static final int APRIL 				= 4;
	public static final int MAY 				= 5;
	public static final int JUNE 				= 6;
	public static final int JULY 				= 7;
	public static final int AUGUST 				= 8;
	public static final int SEPTEMBER 			= 9;
	public static final int OCTOBER 			= 10;
	public static final int NOVEMBER 			= 11;
	public static final int DECEMBER 			= 12;
	public static final int NUMBER_OF_MONTHS 	= 12;
	
	public static final int FEBRUARY_LEAP_YEAR	= 29;
	public static final int FEBRUARY_MAX_DAY	= 28;
	public static final int ODD_MONTH_MAX_DAY	= 31;
	public static final int EVEN_MONTH_MAX_DAY	= 30;
	
	private static final int YEAR_1600 			= 1600;
	private static final int YEAR_1700 			= 1700;
	private static final int YEAR_1800 			= 1800;
	private static final int YEAR_2000 			= 2000;
	private static final int YEAR_2100 			= 2100;
	
	public static final int ZERO 				= 0;
	public static final int ONE 				= 1;
	public static final int TWO 				= 2;
	public static final int THREE 				= 3;
	public static final int FOUR 				= 4;
	public static final int FIVE 				= 5;
	public static final int SIX 				= 6;
	public static final int HUNDRED				= 5;
	public static final int FOUR_HUNDRED		= 6;
	
	/**
	 * @param day
	 * @param month
	 * @param year
	 */
	public Date(int year, int month, int day) {
		setYear(year);
		setMonth(month);
		setDay(day);
	}
	
	/**
	 * @return the day
	 */
	public int getDay() {
		return day;
	}
	
	/**
	 * @return the month
	 */
	public int getMonth() {
		return month;
	}
	
	/**
	 * @return the month
	 */
	public String getMonthName() {
		int month = getMonth();
		String monthName = null;
		switch(month) {
		case 1:
			monthName = "January";
			break;
		case 2:
			monthName =  "Feburary";
			break;
		case 3:
			monthName =  "March";
			break;
		case 4:
			monthName =  "April";
			break;
		case 5:
			monthName =  "May";
			break;
		case 6:
			monthName =  "June";
			break;
		case 7:
			monthName =  "July";
			break;
		case 8:
			monthName =  "August";
			break;
		case 9:
			monthName =  "September";
			break;
		case 10:
			monthName =  "October";
			break;
		case 11:
			monthName =  "November";
			break;
		case 12:
			monthName =  "December";
			break;	
		}
		return monthName;
	}
	
	/**
	 * @return the year
	 */
	public int getYear() {
		return year;
	}
	
	/**
	 * @param day the day to set
	 */
	public void setDay(int day) {
		if(getMonth() % 2 == 0) {
			if(day >= MIN_DAY && day <= EVEN_MONTH_MAX_DAY) {
				this.day = day;
			}else {
				throw new IllegalArgumentException("Invalid day, month " + getMonth() + " does not have " + day + " days.");
			}
		}else if(getMonth() == FEBRUARY){
			if(day >= MIN_DAY && day <= FEBRUARY_MAX_DAY) {
				this.day = day;
			}else {
				throw new IllegalArgumentException("invalid day for feburary");
			}
		}else if(getMonth() == FEBRUARY && isLeapYear() == true){
			if(day >= MIN_DAY && day <= FEBRUARY_LEAP_YEAR) {
				this.day = day;
			}else {
				throw new IllegalArgumentException("invalid day for feburary");
			}
		}else {
			if(day >= MIN_DAY && day <= ODD_MONTH_MAX_DAY) {
				this.day = day;
			}else {
				throw new IllegalArgumentException("Invalid day, month " + getMonth() + " does not have " + day + " days.");
			}
		}
		
	}
	
	/**
	 * @param month the month to set
	 */
	public void setMonth(int month) {
		if(month >= MIN_MONTH && month <= MAX_MONTH) {
			this.month = month;
		}else {
			throw new IllegalArgumentException("invalid month: " + month);
		}
	}
	
	/**
	 * @param year the year to set
	 */
	public void setYear(int year) {
		if(year >= MIN_YEAR && year <= MAX_YEAR) {
			this.year = year;
		}else {
			throw new IllegalArgumentException("invalid year: " + year);
		}
	}
	
	/**
	 * @return the day of the week 
	 */
	public String getDayOfTheWeek() {
		int lastTwoDigitYear = getYear() % LAST_TWO_DIGITS; // gets last 2 digits of year
		int monthCode;							
		
		//determines your month code number, used to determine day of week
		if(getMonth() == JANUARY || getMonth() == NOVEMBER) {
			monthCode = ONE;
		}else if(getMonth() == MAY) {
			monthCode = TWO;
		}else if(getMonth() == AUGUST) {
			monthCode = THREE;
		}else if(getMonth() == FEBRUARY || getMonth() == MARCH || getMonth() == NOVEMBER) {
			monthCode = FOUR;
		}else if(getMonth() == JUNE) {
			monthCode = FIVE;
		}else if(getMonth() == SEPTEMBER || getMonth() == DECEMBER) {
			monthCode = SIX;
		}else {
			monthCode = ZERO;
		}
		
		
		//Algorithm steps to determine what day of the week a given date is
		int step1 = lastTwoDigitYear / NUMBER_OF_MONTHS;
		int step2 = lastTwoDigitYear - step1 * NUMBER_OF_MONTHS;
		int step3 = step2 / FOUR;
		int step4 = getDay();
		int step5 = monthCode;
		
		// subtracts 1 from month code if January and February is in a leap year
		if(getMonth() == JANUARY || getMonth() == FEBRUARY && isLeapYear() == true) {
			step5 -= 1; 
		}
		
		//account for leap years per era
		if(getYear() >= YEAR_1600 && getYear() < YEAR_1700) {
			step5 += SIX;
		}else if(getYear() >= YEAR_1700 && getYear() < YEAR_1800) {
			step5 += FOUR;
		}else if(getYear() >= YEAR_1800 && getYear() < YEAR_2000) {
			step5 += TWO;
		}else if(getYear() >= YEAR_2000 && getYear() < YEAR_2100) {
			step5 += SIX;
		}else if(getYear() >= YEAR_2100) {
			step5 += FOUR;
		}
		
		//gives you the day of the week in a number
		int dayOfTheWeek = (step1 + step2 + step3 + step4 + step5) % 7;
		
		//converts the day of the week number into the name
		String dayOfWeek;
		switch(dayOfTheWeek) {
			case MONDAY:  	dayOfWeek = "Monday";
					 break;
			case TUESDAY:  	dayOfWeek = "Tuesday";
					 break;
			case WEDNESDAY: dayOfWeek = "Wednesday";
					 break;
			case THURSDAY:  dayOfWeek = "Thursday";
					 break;
			case FRIDAY:  	dayOfWeek = "Friday";
					 break;
			case SATURDAY:  dayOfWeek = "Saturday";
					 break;
			case SUNDAY:  	dayOfWeek = "Sunday";
					 break;
			default: 		dayOfWeek = "Invalid day";
					 break;
		}
		return dayOfWeek;
		
	}
	
	/**
	 * @return if the year is a leap year
	 */
	public boolean isLeapYear() {
		boolean isLeapYear = ((getYear() % FOUR == ZERO) && (getYear() % HUNDRED != ZERO) || (getYear() % FOUR_HUNDRED == ZERO)); //leap year algorithm
		return isLeapYear;
	}
	
	/**
	 * @return a string displaying year, month, day, and day of week of the date.
	 */
	public String displayDate() {
		if(getYear() > CURRENT_YEAR) {
			return String.format("The date is " + getMonthName() + " " + getDay() + ", " + getYear() + ". This is a " +  getDayOfTheWeek());
		}else {
			return String.format("The date was " + getMonthName() + " " + getDay() + ", " + getYear() + ". This was a " +  getDayOfTheWeek());
		}
	}
	
	/**
	 * @return a string displaying year, month, day
	 */
	public String display() {
		return String.format(getMonthName() + " " + getDay() + " " + getYear());	
	}
}
