# **Lab 2**
Lab 2 is a follow up to the Date objects made in Lab 1a and is an introductory to if and while statements. The project is a game where the user guesses a random date and receives a score based on their input.
## Date.java
This file contains a class for dates. It has setters and getters to maintain a Date object’s year, month, and day. This class also contains and minimum and maximum for the day, month, and year. Date.java can also check to see if the year set is a leap year.
## Game.java
This file contains 2 games: “Guess the Date Game” and “Guess the Birthday Game”.
### Guess the Date Game
The user is given a random date to guess. If their input is correct, the user receives points. They can stop the game at any time.
### Guess the Birthday Game
The user is given a birth date to guess. If their input is correct, the user receives points. For every wrong answer, the game will provide a hint on the correct date. Users can stop the game at any time.
