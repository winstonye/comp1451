# **Assignment 2**
Assignment 2 is the final assignment for COMP1451. Concepts from labs succeeding lab 5 are covered in this assignment. 

## Objective ##
The objective of assignment 2 is to create multiple vehicles that will be stored within a vehicle collection lot. The focus of the assignment is on the testing components using junit, which must made before any vehicle is made.

## Vehicle Types ##
The following vehicle types were made:
* Airplanes

* Boats

* Cars

## VehicleCollection.java ##
VehicleCollection.java is an object that will store all planes, cars, and boats within their corresponding list.

## CarTest.java ##
Given an input of a car with a brand (string, ex: Mazda"), a car model (string, ex: "Miata"), year model (integer, ex: 1998), and a horsepower (integer, ex: 110), tests are used to assert validity of a plane.

Assertions are made to test:

* a valid horsepower value (max, min)

* year of manufacturing (max, min)

* a valid model

* a valid make (given all parameters)

* a valid comparison of two cars


## AirplaneTest.java ##
Given an input of a airplane with a year manufactured (integer), make (string), model (string), maximum height (int), tests are used to assert a validity of a plane.

Assertions are made to test:

* a valid year manufactured (max, min)

* a valid maximum height (max, min)

* a valid model

* a valid make (given all parameters)

* a valid comparison of two planes


## BoatTest.java ##
Given an input of a boat with a year manufactured (integer), model (string), and a boolean for being motorized, tests are used to assert a validity of a boat.

Assertions are made to test:

* a valid year manufactured (max, min)

* a valid make (given all parameters)

* a valid value for being motorized

* a valid comparison of two boats