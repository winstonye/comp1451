/**
 * 
 */
package ca.bcit.comp1451.assignment2.masonliang;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.jupiter.api.Test;

/**
 * Car test
 * @author Mason
 *
 */
public class CarTest {
	private Car v =  new Car(1998, "Mazda", "Miata", 110);
	private Car v2 = new Car(2018, "Nissan", "GTR", 565);
	
	@Test
	public void testConstructor() {
		new Car(1998, "Mazda", "Miata", 110);
		
	}
	
	@Test
	public void testConstructor2() {
		new Car(2018, "Nissan", "GTR", 565);
	}
	
	@Test
	public void testGetHorsepower() {
		assertEquals(110, v.getHorsepower());
	}
	
	@Test
	public void testGetHorsepower2() {
		assertEquals(565, v2.getHorsepower());
	}
	
	@Test
	public void testBadSetHorsepowerOver() {
		try {
			new Car(1998, "Mazda", "Miata", 2000000);
			fail("hp over 11000 or less than 1 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid horsepower",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetHorsepowerLessThan() {
		try {
			new Car(2018, "Nissan", "GTR", 0);
			fail("hp over 11000 or less than 1 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid horsepower",  e.getMessage());
		}
	}
	
	@Test
	public void testGetYearManufacturered() {
		assertEquals(1998, v.getYearManufactured());
	}
	
	@Test
	public void testGetYearManufacturered2() {
		assertEquals(2018, v2.getYearManufactured());
	}
	
	@Test
	public void testBadSetYearManufactureredOver() {
		try {
			new Car(2020, "Mazda", "Miata", 110);
			fail("year over 2019 or less than 1884 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid year",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetYearManufactureredLessThan() {
		try {
			new Car(1883, "Nissan", "GTR", 565);
			fail("year over 2018 or less than 1884 must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid year",  e.getMessage());
		}
	}
	
	@Test
	public void testGetMake() {
		assertEquals("Mazda", v.getMake());
	}
	
	@Test
	public void testGetMake2() {
		assertEquals("Nissan", v2.getMake());
	}
	
	@Test
	public void testBadSetMakeNull() {
		try {
			new Car(1998, null, "Miata", 110);
			fail("null or empty make must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid make",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetMakeEmpty() {
		try {
			new Car(1998, "", "Miata", 110);
			fail("null or empty make must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid make",  e.getMessage());
		}
	}
	
	@Test
	public void testGetModel() {
		assertEquals("Mazda", v.getMake());
	}
	
	@Test
	public void testGetModel2() {
		assertEquals("Nissan", v2.getMake());
	}
	
	@Test
	public void testBadSetModelNull() {
		try {
			new Car(1998, "Mazda", null, 110);
			fail("null or empty model must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid model",  e.getMessage());
		}
	}
	
	@Test
	public void testBadSetModelEmpty() {
		try {
			new Car(1998, "Mazda", "", 110);
			fail("null or empty model must throw IllegalArgumentException");
		}catch (Exception e) {
			assertEquals("invalid model",  e.getMessage());
		}
	}
	
	@Test
	public void testMotorized() {
		assertEquals(110, v.getHorsepower());
	}
	
	@Test
	public void testMotorized2() {
		assertEquals(565, v2.getHorsepower());
	}
	
	
	@Test
	public void testToString() {
		assertEquals("This car is a 1998 Mazda Miata with 110 hp.", v.toString());
	}
	
	@Test
	public void testToString2() {
		assertEquals("This car is a 2018 Nissan GTR with 565 hp.", v2.toString());
	}
	
	@Test
	public void testEqualsTrue() {
		Car v  =  new Car(1998, "Mazda", "Miata", 550);
		Car v2  = new Car(1998, "Mazda", "Miata", 560);
		
		assertTrue(v.equals(v2) && v2.equals(v));
	}
	
	@Test
	public void testEqualsFalse() {
		assertFalse(v.equals(v2) && v2.equals(v));
		assertFalse(v.hashCode() == v2.hashCode());
	}
	
	@Test
	public void testCompareTo() {
		Car v   = new Car(1998, "Mazda", "Miata", 550);
		Car v2  = new Car(1998, "Mazda", "RX-8", 550);
		
		assertTrue(v.compareTo(v2) == 0);
	}
	
	@Test
	public void testCompareToNewer() {
		Car v   = new Car(1998, "Mazda", "Miata", 550);
		Car v2  = new Car(2018, "Nissan", "GTR", 565);
		
		assertTrue(v.compareTo(v2) < 0);
	}
}
