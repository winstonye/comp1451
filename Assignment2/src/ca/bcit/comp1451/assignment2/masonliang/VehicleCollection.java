package ca.bcit.comp1451.assignment2.masonliang;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

/**
 * 
 */


/**
 * A collection of vehicles
 * @author Mason
 *
 */
public class VehicleCollection {
	List<Car> cars;
	List<Boat> boats;
	List<Airplane> airplanes;
	
	/**
	 * Creates a arraylist of cars and sorts them
	 * Cars with higher horsepower are considered better
	 */
	public void createCollectionCars() {
		cars = new ArrayList<>();
		
		int yearManufacturered = 2000;
		String make = "Lamborghini";
		String model = "Diablo";
		int horsepower = 770;
		Vehicle v = new Car(yearManufacturered, make, model, horsepower);
		cars.add((Car) v);
		
		yearManufacturered = 1997;
		make = "Dodge";
		model = "Ram";
		horsepower = 175;
		v = new Car(yearManufacturered, make, model, horsepower);
		cars.add((Car) v);
		
		yearManufacturered = 1940;
		make = "Buggati";
		model = "Veyron";
		horsepower = 135;
		v = new Car(yearManufacturered, make, model, horsepower);
		cars.add((Car) v);
		
		yearManufacturered = 2014;
		make = "Honda";
		model = "Civic";
		horsepower = 143;
		v = new Car(yearManufacturered, make, model, horsepower);
		cars.add((Car) v);
		
		yearManufacturered = 2011;
		make = "Honda";
		model = "Civic";
		horsepower = 143;
		v = new Car(yearManufacturered, make, model, horsepower);
		cars.add((Car) v);
		
		yearManufacturered = 1999;
		make = "Toyota";
		model = "Corolla";
		horsepower = 140;
		v = new Car(yearManufacturered, make, model, horsepower);
		cars.add((Car) v);
		
		Collections.sort(cars);
		
		for(Car car : cars) {
			System.out.println(car.toString());
		}
	}
	
	/**
	 * Crates a arraylist of boats and sorts them
	 * Boats that are newer are better
	 */
	public void createCollectionBoats() {
		boats = new ArrayList<>();
		
		int yearManufacturered = 1980;
		String make = "Bayliner";
		String model = "Extreme";
		boolean moterized = true;
		Vehicle v = new Boat(yearManufacturered, make, model, moterized);
		boats.add((Boat) v);
		
		yearManufacturered = 2014;
		make = "Bayliner";
		model = "Extreme II";
		moterized = true;
		v = new Boat(yearManufacturered, make, model, moterized);
		boats.add((Boat) v);
		
		yearManufacturered = 2000;
		make = "American Skier";
		model = "Skier Surpreme";
		moterized = false;
		v = new Boat(yearManufacturered, make, model, moterized);
		boats.add((Boat) v);
		
		yearManufacturered = 2010;
		make = "Boesch";
		model = "Journey";
		moterized = false;
		v = new Boat(yearManufacturered, make, model, moterized);
		boats.add((Boat) v);
		
		Collections.sort(boats);
		
		for(Boat boat : boats) {
			System.out.println(boat.toString());
		}
	}
	
	/**
	 * Creates a arraylist of airplanes and sorts it.
	 * Airplanes that can fly higher are better
	 */
	public void createCollectionAirplanes() {
		airplanes = new ArrayList<>();
		
		int yearManufacturered = 1998;
		String make = "ABC Motors";
		String model = "Comac";
		int maxHeightFeet = 10000;
		Vehicle v = new Airplane(yearManufacturered, make, model, maxHeightFeet);
		airplanes.add((Airplane) v);
		
		yearManufacturered = 1940;
		make = "Boeing";
		model = "84";
		maxHeightFeet = 45000;
		v = new Airplane(yearManufacturered, make, model, maxHeightFeet);
		airplanes.add((Airplane) v);
		
		yearManufacturered = 2012;
		make = "Boeing";
		model = "747";
		maxHeightFeet = 80000;
		v = new Airplane(yearManufacturered, make, model, maxHeightFeet);
		airplanes.add((Airplane) v);
		
		yearManufacturered = 2014;
		make = "Abrams";
		model = "Motorhead";
		maxHeightFeet = 70000;
		v = new Airplane(yearManufacturered, make, model, maxHeightFeet);
		airplanes.add((Airplane) v);
		
		Collections.sort(airplanes);
		
		for(Airplane airplane : airplanes) {
			System.out.println(airplane.toString());
		}
	}
}
