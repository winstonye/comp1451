package ca.bcit.comp1451.assignment2.masonliang;
/**
 * 
 */


/**
 * This class defines a boat
 * @author Mason
 *
 */
public class Boat extends Vehicle implements Comparable<Boat>, Steerable{
	private boolean motorized;
	
	private static final int MIN_YEAR = 0;
	private static final int MAX_YEAR = 2018;

	/**
	 * @param yearManufactured Model year of boat 
	 * @param make Make of boat
	 * @param model Model of boat 
	 * @param motorized Does the boat has a motor?
	 */
	public Boat(int yearManufactured, String make, String model, boolean motorized) {
		super(yearManufactured, make, model);
		this.motorized = motorized;
	}

	/**
	 * @return the motorized
	 */
	public boolean getMotorized() {
		return motorized;
	}

	@Override
	/**
	 * @param yearManufactured the yearManufactured to set
	 */
	public void setYearManufactured(int yearManufactured) {
		if(yearManufactured <= MIN_YEAR || yearManufactured > MAX_YEAR) {
			throw new IllegalArgumentException("invalid year");
		}
		super.setYearManufactured(yearManufactured);;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		if(getMotorized() == true) {
			return "This " + getClass().getSimpleName().substring(0).toLowerCase() + " is a " + getYearManufactured() + " " + getMake() + " " + getModel() + " (with motor).";
		}
		return "This " + getClass().getSimpleName().substring(0).toLowerCase() + " is a " + getYearManufactured() + " " + getMake() + " " + getModel() + " (without motor).";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + (motorized ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Boat))
			return false;
		Boat other = (Boat) obj;
		if (getMotorized() == other.getMotorized())
			return true;
		if (getMotorized() != other.getMotorized())
			return false;
		return true;
	}
	
	/**
	 * Boats that are newer are better
	 */
	@Override
	public int compareTo(Boat other) {
		return Integer.compare(this.getYearManufactured(), other.getYearManufactured());
	}
	
	@Override
	public void accelerate() {
		System.out.println("jet water");
	}
	
	@Override
	public void steerRight() {
		System.out.println("turn tiller right");
	}
	
	@Override
	public void steerLeft() {
		System.out.println("turn tiller left");
	}
}
