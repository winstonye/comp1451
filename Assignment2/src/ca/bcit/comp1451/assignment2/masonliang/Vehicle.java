package ca.bcit.comp1451.assignment2.masonliang;
/**
 * 
 */


/**
 * This class defines a vehicle
 * @author liang
 *
 */
public abstract class Vehicle {
	private int yearManufactured;
	private String make;
	private String model;
	
	/**
	 * Empty constructor for testing
	 */
	public Vehicle() {};
	
	/**
	 * @param yearManufactured Model year of the vehicle
	 * @param make Make of the vehicle
	 * @param model Model of the vehicle
	 */
	public Vehicle(int yearManufactured, String make, String model) {
		setYearManufactured(yearManufactured);
		setMake(make);
		setModel(model);
	}

	/**
	 * @return the yearManufactured
	 */
	public int getYearManufactured() {
		return yearManufactured;
	}
	
	/**
	 * @param yearManufactured the yearManufactured to set
	 */
	public void setYearManufactured(int yearManufactured) {
		if(yearManufactured <= 0 || yearManufactured > 9999) {
			throw new IllegalArgumentException("invalid year");
		}
		this.yearManufactured = yearManufactured;
	}
	
	/**
	 * @return the make
	 */
	public String getMake() {
		return make;
	}
	
	/**
	 * @param make the make to set
	 */
	public void setMake(String make) {
		if(make == null || make.isEmpty()) {
			throw new IllegalArgumentException("invalid make");
		}
		this.make = make;
	}
	
	/**
	 * @return the model
	 */
	public String getModel() {
		return model;
	}
	
	/**
	 * @param model the model to set
	 */
	public void setModel(String model) {
		if(model == null || model.isEmpty()) {
			throw new IllegalArgumentException("invalid model");
		}
		this.model = model;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "This vehicle is a " + getYearManufactured() + " " + getMake() + " " + getModel() + ".";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((make == null) ? 0 : make.hashCode());
		result = prime * result + ((model == null) ? 0 : model.hashCode());
		result = prime * result + yearManufactured;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Vehicle))
			return false;
		Vehicle other = (Vehicle) obj;
		if (make == null) {
			if (other.make != null)
				return false;
		} else if (!make.equals(other.make))
			return false;
		if (model == null) {
			if (other.model != null)
				return false;
		} else if (!model.equals(other.model))
			return false;
		if (yearManufactured != other.yearManufactured)
			return false;
		return true;
	}
	
	
}
