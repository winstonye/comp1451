package ca.bcit.comp1451.assignment2.masonliang;
/**
 * 
 */

/**
 * @author Mason
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		VehicleCollection c = new VehicleCollection();
		c.createCollectionCars();
		System.out.println();
		c.createCollectionBoats();
		System.out.println();
		c.createCollectionAirplanes();
	}

}
