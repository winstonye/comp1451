package ca.bcit.comp1451.assignment2.masonliang;
/**
 * 
 */



/**
 * This class defines a car
 * @author Mason
 *
 */
public class Car extends Vehicle implements Comparable<Car>, Steerable{
	private int horsepower;
	
	private static final int MIN_YEAR = 1884;
	private static final int MAX_YEAR = 2020;
	
	private static final int MIN_HP = 1;
	private static final int MAX_HP = 11000;
	
	private static final int HP_RANGE = 10;

	/**
	 * @param yearManufactured Model year of car
	 * @param make Make of car
	 * @param model Model of car
	 * @param horsepower Horsepower of car
	 */
	public Car(int yearManufactured, String make, String model, int horsepower) {
		super(yearManufactured, make, model);
		setHorsepower(horsepower);
	}

	/**
	 * @return the horsepower
	 */
	public int getHorsepower() {
		return horsepower;
	}

	/**
	 * @param horsepower the horsepower to set
	 */
	public void setHorsepower(int horsepower) {
		if(horsepower < MIN_HP || horsepower >= MAX_HP) {
			throw new IllegalArgumentException("invalid horsepower");
		}
		this.horsepower = horsepower;
	}
	
	@Override
	/**
	 * @param yearManufactured the yearManufactured to set
	 */
	public void setYearManufactured(int yearManufactured) {
		if(yearManufactured <= MIN_YEAR || yearManufactured >= MAX_YEAR) {
			throw new IllegalArgumentException("invalid year");
		}
		super.setYearManufactured(yearManufactured);;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "This " + getClass().getSimpleName().substring(0).toLowerCase() + " is a " + getYearManufactured() + " " + getMake() + " " + getModel() + " with " + getHorsepower() + " hp.";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + horsepower;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof Car))
			return false;
		Car other = (Car) obj;
		if (Math.abs(this.getHorsepower() - ((Car) obj).getHorsepower()) <= HP_RANGE)
			return true;
		if (getHorsepower() != ((Car) obj).getHorsepower()) 
			return false;
		return true;
	}

	/**
	 * Cars with more horsepower are better
	 */
	@Override
	public int compareTo(Car other) {
		return this.getHorsepower() - other.getHorsepower();
	}
	
	@Override
	public void accelerate() {
		System.out.println("fire pistons, turn wheels");
	} 
	
	@Override
	public void steerRight() {
		System.out.println("turn wheels right");
	}
	
	@Override
	public void steerLeft() {
		System.out.println("turn wheels left");
	}
}
