package ca.bcit.jasonharrison;

public interface Speakable {
	// automatically public and abstract:
	void speak();
	void whisper();
	void shout();
	
	default void foo() {}
	
	default String getPurpose() {
		return "to communicate";
	}
}
