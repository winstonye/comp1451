package ca.bcit.jasonharrison;

public class Dog extends Animal{
	
	public Dog(int kg) {
		super(kg);
	}
	
	@Override
	public void speak() {
		System.out.println("woof");
	}
	
	@Override
	public void whisper() {
		System.out.println("wuf");
	}	
	
	@Override
	public void shout() {
		System.out.println("grrr");
	}	
	
	@Override
	public void move(int speedKmPerHour) {
		System.out.println("run at " + speedKmPerHour + "km/hr");
	}
	

}
