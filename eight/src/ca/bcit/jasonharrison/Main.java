package ca.bcit.jasonharrison;

import java.util.ArrayList;
import java.util.Collections;

public class Main {
	public static void main(String[] args) {

		ArrayList<Animal> animals = new ArrayList<>();
		
		animals.add(new Dog(40));
		animals.add(new Dog(80));
		animals.add(new Cat(2));
		animals.add(new Dog(123));
		animals.add(new Dog(6));
		animals.add(new Cat(14));
		animals.add(new Dog(54));
		
		System.out.println(animals);

		Collections.sort(animals);
		
		System.out.println(animals);
		
	}
}
