package ca.bcit.jasonharrison;

public interface Moveable {
	public abstract void move(int speedKmPerHr);
	
	
	default void foo() {}
}
