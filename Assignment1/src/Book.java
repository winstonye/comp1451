
/**
 * 
 */

/**
 * This is a book class
 * @author Mason
 *
 */
public class Book extends Item{
	private String title;
	private Author author;
	private Date datePublished;
	private BookType genre;
	

	/**
	 * @param weightKg
	 * @param manufacturingPriceDollars
	 * @param suggestedRetailPriceDollars
	 * @param uniqueID
	 * @param author
	 * @param datePublished
	 * @param genre
	 */
	public Book(double weightKg, double manufacturingPriceDollars, double suggestedRetailPriceDollars, String uniqueID,
			Author author, Date datePublished, String title, BookType genre) {
		super(weightKg, manufacturingPriceDollars, suggestedRetailPriceDollars, uniqueID);
		setAuthor(author);
		setDatePublished(datePublished);
		setGenre(genre);
		setTitle(title);
	}
	
	/**
	 * @return title of book
	 */
	public String getTitle() {
		return title;
	}
	
	/**
	 * @return the author
	 */
	public Author getAuthor() {
		return author;
	}

	/**
	 * @return the datePublished
	 */
	public Date getDatePublished() {
		return datePublished;
	}

	/**
	 * @return the genre
	 */
	public BookType getGenre() {
		return genre;
	}
	
	/**
	 * Calls get method for uniqueID in Item class
	 * @return ISBN
	 */
	public String getISBN() {
		return getUniqueID();
	}
	
	/**
	 * Gives you author first name
	 * @return first name string
	 */
	public String getAuthorFirstName() {
		return author.getName().getFirstName();
	}
	
	/**
	 * Gives you a genre in a string
	 * @return genre
	 */
	public String getGenreString() {
		return genre.getTheBookType();
	}
	
	/**
	 * @param title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * @param author the author to set
	 */
	public void setAuthor(Author author) {
		this.author = author;
	}

	/**
	 * @param datePublished the datePublished to set
	 */
	public void setDatePublished(Date datePublished) {
		this.datePublished = datePublished;
	}

	/**
	 * @param genre the genre to set
	 */
	public void setGenre(BookType genre) {
		this.genre = genre;
	}
	
	/**
	 * Sets ISBN to book, calls setUniqueID on Book class
	 * @param uniqueID your ISBN number
	 */
	public void setISBN(String uniqueID) {
		setUniqueID(uniqueID);
	}
	
	
}
