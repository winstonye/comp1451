/**
 * 
 */

/**
 * @author liang
 *
 */
public class Address {
	private String streetNumber;
	private String streetName;
	private String city;
	private String postalCode;
	
	/**
	 * @param streetNumber Street number
	 * @param streetName Street name
	 * @param city City 
	 * @param postalCode Postal code
	 */
	public Address(String streetNumber, String streetName, String city, String postalCode) {
		setStreetNumber(streetNumber);
		setStreetName(streetName);
		setCity(city);
		setPostalCode(postalCode);
	}

	/**
	 * @return the streetNumber
	 */
	public String getStreetNumber() {
		return streetNumber;
	}

	/**
	 * @return the streetName
	 */
	public String getStreetName() {
		return streetName;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param streetNumber the streetNumber to set
	 */
	public void setStreetNumber(String streetNumber) {
		this.streetNumber = streetNumber;
	}

	/**
	 * @param streetName the streetName to set
	 */
	public void setStreetName(String streetName) {
		this.streetName = streetName;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	
	
}
