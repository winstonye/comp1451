
/**
 * 
 */

/**
 * This class defines a author
 * @author Mason
 *
 */
public class Author {
	private Date birthDate;
	private Name name;
	private BookType genre;
	private String pseudonym;
	
	/**
	 * @param birthDate birth date of author
	 * @param name of author
	 * @param genre of author's book
	 * @param pseudonym or author
	 */
	public Author(Date birthDate, Name name, BookType genre, String pseudonym) {
		setBirthDate(birthDate);
		setName(name);
		setGenre(genre);
		setPseudonym(pseudonym);
	}
	
	/**
	 * @param birthDate of author
	 * @param name of author's book
	 * @param genre of book
	 */
	public Author(Date birthDate, Name name, BookType genre) {
		setBirthDate(birthDate);
		setName(name);
		setGenre(genre);
	}

	/**
	 * @return the birthDate
	 */
	public Date getBirthDate() {
		return birthDate;
	}

	/**
	 * @return the name
	 */
	public Name getName() {
		return name;
	}

	/**
	 * @return the genre
	 */
	public BookType getGenre() {
		return genre;
	}

	/**
	 * @return the pseudonym
	 */
	public String getPseudonym() {
		return pseudonym;
	}

	/**
	 * @param birthDate the birthDate to set
	 */
	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(Name name) {
		this.name = name;
	}

	/**
	 * @param genre the genre to set
	 */
	public void setGenre(BookType genre) {
		this.genre = genre;
	}

	/**
	 * @param pseudonym the pseudonym to set
	 */
	public void setPseudonym(String pseudonym) {
		if(pseudonym != null) {
			this.pseudonym = pseudonym;
		}
	}
	
	
}
