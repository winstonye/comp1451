import java.awt.Color;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

/**
 * 
 */

/**
 * This class defines a shoe store
 * @author liang
 *
 */
public class ShoeStore extends Store{
	ShoeType department;

	/**
	 * @param streetAddress of store
	 * @param name of store
	 * @param department of store
	 */
	public ShoeStore(Address streetAddress, String name, ShoeType department) {
		super(streetAddress, name);
		this.department = department;
		addShoes();
	}
	
	/**
	 * @param streetAddress of store
	 * @param name of store
	 * @param department of store
	 */
	public ShoeStore(Address streetAddress, String name, String department) {
		super(streetAddress, name);
		this.department = ShoeType.get(department);
		addShoes();
	}
	
	/**
	 * Add shoes into item list
	 */
	public void addShoes() {
		Name designer = new Name("Skechers");
		int size = 10;
		Material material = Material.get("leather");
		Color color = Color.DARK_GRAY;
		department = ShoeType.MEN;
		Shoe s = new Shoe(1, 58.5, 90.0, "Diameter", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Robert", "Cobbler");
		size = 12;
		material = Material.get("leather");
		color = Color.BLACK;
		department = ShoeType.DRESS;
		s = new Shoe(1.15, 104, 160, "Wave", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Geox");
		size = 7;
		material = Material.get("cloth");
		color = Color.BLUE;
		department = ShoeType.MEN;
		s = new Shoe(1, 110.5, 170, "Monet", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Nine", "West");
		size = 8;
		material = Material.get("plastic");
		color = Color.BLACK;
		department = ShoeType.WOMEN;
		s = new Shoe(0.85, 84.5, 130, "Camya Multi Glitter", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Geox");
		size = 10;
		material = Material.get("plastic");
		color = Color.GRAY;
		department = ShoeType.WOMEN;
		s = new Shoe(0.9, 97.5, 150, "Marieclaire", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Stride", "Rite");
		size = 9;
		material = Material.get("rubber");
		color = Color.GRAY;
		department = ShoeType.CHILDREN;
		s = new Shoe(0.6, 45.5, 70, "Balance Of The Force", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Sperry");
		size = 9;
		material = Material.get("cloth");
		color = Color.ORANGE;
		department = ShoeType.CHILDREN;
		s = new Shoe(0.7, 39, 60, "Top-Sider Unisex Bluefish H&L", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Skechers");
		size = 10;
		material = Material.get("plastic");
		color = Color.PINK;
		department = ShoeType.CHILDREN;
		s = new Shoe(0.85, 32.5, 50, "Lite Kicks Rainbow Sprite", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Robert", "Cobbler");
		size = 5;
		material = Material.get("cloth");
		color = Color.BLUE;
		department = ShoeType.CHILDREN;
		s = new Shoe(0.5, 39, 60, "Toachi", designer, size, material, color, department);
		addItem(s);
		
		designer = new Name("Nike");
		size = 13;
		material = Material.get("rubber");
		color = Color.WHITE;
		department = ShoeType.SPORTS;
		s = new Shoe(1.2, 117, 180, "Jordan Ace 23 II", designer, size, material, color, department);
		addItem(s);
	}
	
	/**
	 * Gives you all shoes and designers
	 */
	public void displayAllShoesAndDesigners() {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		
		while(it.hasNext()) {
			Shoe s = it.next();
			System.out.println(s.getDesigner().fullName().trim() + " offers " + s.getMaterial().getTheMaterial() + " size-" + s.getSize() + " " + s.getShoeType().getTheShoeType() +"'s " + s.getDescription());
		}
	}
	
	/**
	 * Gives you all shoes by designer
	 * @param designerName name
	 */
	public void displayAllShoesByDesigner(String designerName) {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		
		while(it.hasNext()) {
			Shoe s = it.next();
			if(s.getDesigner().fullName().trim().equals(designerName) && designerName != null) {
				System.out.println(s.getDesigner().fullName().trim() + " offers a size-" + s.getSize() + " " + s.getShoeType().getTheShoeType() + "'s " + s.getDescription());
			}
		}
	}
	
	/**
	 * Gives you all shoes with inputed material
	 * @param material enum
	 */
	public void displayAllShoesMadeOf(Material material) {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		boolean displayedSome = false;
		
		while(it.hasNext()) {
			Shoe s = it.next();
			if(s.getMaterial().equals(material) && material != null) {
				System.out.println("The size-" + s.getSize() + " " + s.getDescription() + " is made of " + s.getMaterial().getTheMaterial());
				displayedSome = true;
			}
		}
		if(displayedSome = false) {
			System.out.println("No shoes made from " + material);
		}
	}
	
	/**
	 * Gives you all shoes with inputed material string
	 * @param material string
	 */
	public void displayAllShoesMadeOf(String material) {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		boolean displayedSome = false;
		
		while(it.hasNext()) {
			Shoe s = it.next();
			if(s.getMaterial().getTheMaterial().equals(material) && material != null) {
				System.out.println("The size-" + s.getSize() + " " + s.getDescription() + " is made of " + s.getMaterial().getTheMaterial());
				displayedSome = true;
			}
		}
		if(displayedSome == false) {
			System.out.println("No shoes made from " + material);
		}
	}
	
	/**
	 * Gives you shoe designed by input
	 * @param designer name
	 */
	public void displayNumberOfShoesDesignedBy(Name designer) {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		int numberOfShoesByDesigner = 0;
		
		if(designer == null) {
			System.out.println("name is null");
			return;
		}
		
		while(it.hasNext()) {
			Shoe s = it.next();
			if(s.getDesigner().fullName().equals(designer.fullName()) && designer != null) {
				numberOfShoesByDesigner++;
			}
		}
		System.out.println("This store has " + numberOfShoesByDesigner + " shoes designed by " + designer.fullName().trim());
	}
	
	/**
	 * Gives you shoe designed by last name of designer
	 * @param desginerLastName last name of designer
	 */
	public void displayNumberOfShoesDesignedBy(String desginerLastName) {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		int numberOfShoesDesginedBy = 0;
		
		if(desginerLastName == null) {
			System.out.println("desginer name is null");
			return;
		}
		while(it.hasNext()) {
			Shoe s = it.next();
			if(s.getDesigner().getLastName() == (desginerLastName)) {
				numberOfShoesDesginedBy++;
			}
		}
		System.out.println("This store has " + numberOfShoesDesginedBy + " shoes designed by " + desginerLastName);
	}
	
	/**
	 * Gives you smallest shoe
	 */
	public void displaySmallestShoeSize() {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		int smallestShoeSize = 0;
		Shoe s = it.next();
		
		while(it.hasNext()) {
			smallestShoeSize = it.next().getSize();
			if(s.getSize() < smallestShoeSize) {
				smallestShoeSize = s.getSize();
			}
		}
		System.out.println("smallest shoe size: " + smallestShoeSize);
	}
	
	/**
	 * Gives you total weight of all shoes
	 */
	public void displayTotalWeightKgOfAllShoes() {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		double sumOfShoesWeightKg = 0.0;
		
		while(it.hasNext()) {
			Shoe s = it.next();
			sumOfShoesWeightKg += s.getWeightKg();
		}
		System.out.println("total kg of shoes: " + sumOfShoesWeightKg);
	}
	
	/**
	 * Gives you shoes that matches inputed param
	 * @param m material of shoe
	 * @param desginer name of shoe
	 */
	public void displayAllShoesOfThisMaterialMadeByThisDesigner(Material m, Name desginer) {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		boolean displayedSome = false;
		
		if(m == null || desginer == null) {
			System.out.println("material or name is null");
			return;
		}
		while(it.hasNext()) {
			Shoe s = it.next();
			if(s.getDesigner().fullName().equals(desginer.fullName()) && s.getMaterial() == m) {
				displayedSome = true;
				System.out.println("The " + s.getDescription() + " is a " + s.getMaterial().getTheMaterial() + " shoe offered by " + s.getDesigner().fullName().trim());
			}
		}
		if(displayedSome == false) {
			System.out.println("This store has no " + m.getTheMaterial() + " shoes designed by " + desginer.fullName().trim());
		}
	}
	
	/**
	 * Gives you all shoes not in matching store
	 */
	public void displayAllShoesNotInMatchingStore() {
		Collection<Shoe> shoes = getCollectionOfItems();
		Iterator<Shoe> it = shoes.iterator();
		
		while(it.hasNext()) {
			Shoe s = it.next();
			if(!s.getShoeType().equals(department)) {
				System.out.println("The " + s.getDescription() + " is a " + s.getShoeType() + "'s shoe offered by " + s.getDesigner().fullName().trim());
			}
		}
	}
}
