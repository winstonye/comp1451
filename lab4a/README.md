# **Lab 4a**
Lab 4a is an introductory lab to assertion testing using mathematical equations. Assertion tests were made before any of the mathematical functions.
## Mathematics.java ##
This file contains functions for calculating the area of a circle, calculating the area of a square, adding, subtracting, multiplying, finding absolute value, and converting feet to kilometers. All functions utilize doubles (64-bit floating point values) except absoluteValue and divide.
## MathematicsTest.java ##
This file contains assertion test cases for all the functions found within Mathematics.java