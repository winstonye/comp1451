package ca.bcit.comp1451.skunk.masonliang;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Skunk skunk = new Skunk();
		Scanner s = new Scanner(System.in);
		
		System.out.println("Play with 2 or 3 dices");
		int input = s.nextInt();
		if (input == 2) {
			skunk.playSkunkTwoDices();
		} else if (input == 3) {
			skunk.playSkunkThreeDices();
		} else {
			System.out.println("invalid input");
		}
		s.close();
	}

}
