/**
 * 
 */
package ca.bcit.comp1451.lab4b.masonliang;

/**
 * This class defines a bank account
 * @author Mason Liang
 *
 */

public class BankAccount {
	private String firstName;
	private String lastName;
	private int PIN;
	private double balanceUSD;
	private int yearOpened;
	
	
	/**
	 * @param firstName First name of client
	 * @param lastName	Last name of client
	 * @param pIN Personal identification number
	 * @param balanceUSD Balance in USD
	 * @param yearOpened Year when account opened 
	 */
	public BankAccount(String firstName, String lastName, int PIN, double balanceUSD, int yearOpened) {
		if(firstName == null || firstName.trim().isEmpty() || firstName.isEmpty()) {
			throw new IllegalArgumentException("first name cannot be null or empty");
		}else {
			this.firstName = firstName;
		}

		if(lastName == null || lastName.trim().isEmpty() || lastName.isEmpty()) {
			throw new IllegalArgumentException("last name cannot be null or empty");
		}else {
			this.lastName = lastName;
		}
		setPIN (PIN);

		if(balanceUSD < 0) {
			throw new IllegalArgumentException("balance has to be greater than 0");
		}else {
			this.balanceUSD = balanceUSD;
		}
		
		if(yearOpened > 2018) {
			throw new IllegalArgumentException("inputted year greater than current year");
		}else if(yearOpened < 0) {
			throw new IllegalArgumentException("year cannot be negative");
		}else {
			this.yearOpened = yearOpened;
		}
	}
	
	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}
	
	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}
	
	/**
	 * @return the pIN
	 */
	public int getPIN() {
		return PIN;
	}
	
	/**
	 * @return the balanceUSD
	 */
	public double getBalanceUSD() {
		return balanceUSD;
	}
	
	/**
	 * @return the yearOpened
	 */
	public int getYearOpened() {
		return yearOpened;
	}
	
	/**
	 * @param pIN the pIN to set
	 */
	public void setPIN(int PIN) {
		if(PIN > 9999 || PIN < 0) {
			throw new IllegalArgumentException("pin must be 4 digits or less");
		}else {
			this.PIN = PIN;
		}
		
	}
	
	/**
	 * @param balanceUSD the balanceUSD to set
	 */
	private void setBalanceUSD(double balanceUSD) {
		this.balanceUSD = balanceUSD;
	}
	
	/**
	 * Withdraw method
	 * @param amount to withdraw in USD
	 */
	public void withdraw(double amount) {
		if(amount < 0) {
			throw new IllegalArgumentException("you cannot withdraw a negative amount");
		}else if(amount == 0){
			throw new IllegalArgumentException("you cannot withdraw nothing");
		}else {
			setBalanceUSD(getBalanceUSD() - amount);
		}
	}
	
	/**
	 * Deposit method
	 * @param amount to deposit in USD
	 */
	public void deposit(double amount) {
		if(amount < 0) {
			throw new IllegalArgumentException("you cannot deposit a negative amount");
		}else if(amount == 0){
			throw new IllegalArgumentException("you cannot deposit nothing");
		}else {
			setBalanceUSD(getBalanceUSD() + amount);
		}
	}  
}
