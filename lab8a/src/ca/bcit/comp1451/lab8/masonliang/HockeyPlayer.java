/**
 * 
 */
package ca.bcit.comp1451.lab8.masonliang;

/**
 * The class defines a Hockey Player
 * @author Mason
 *
 */
public class HockeyPlayer extends Employee implements Comparable<HockeyPlayer>{
	private int numberOfGoals;
	private static final double OVER_TIME_PAY_RATE = 0.0;
	
	/**
	 * @parama name
	 * @param numberOfGoals
	 */
	public HockeyPlayer(String name, int numberOfGoals) {
		super(name);
		setNumberOfGoals(numberOfGoals);
	}
	
	/**
	 * @return the numberOfGoals
	 */
	public int getNumberOfGoals() {
		return numberOfGoals;
	}

	/**
	 * @param numberOfGoals the numberOfGoals to set
	 */
	public void setNumberOfGoals(int numberOfGoals) {
		this.numberOfGoals = numberOfGoals;
	}

	/**
	 * @return Dress code
	 */
	@Override
	public String getDressCode() {
		return "jeresy";
	}
	
	/**
	 * @return Is employee paid salary?
	 */
	@Override
	public boolean isPaidSalary() {
		return true;
	}
	
	/**
	 * @return Does job require a post secondary education?
	 */
	@Override
	public boolean postSecondaryEducationRequired() {
		return true;
	}
	
	/**
	 * @return Work
	 */
	@Override
	public String getWorkVerb() {
		return "play";
	}
	
	/**
	 * @return Overtime rate
	 */
	@Override
	public double getOverTimePayRate() {
		return OVER_TIME_PAY_RATE;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numberOfGoals;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof HockeyPlayer))
			return false;
		HockeyPlayer other = (HockeyPlayer) obj;
		if (numberOfGoals != other.numberOfGoals)
			return false;
		if (numberOfGoals == other.numberOfGoals)
			return false;
		return true;
	}
	/**
	 * Override more number of goals, better hockey player
	 */
	@Override
	public int compareTo(HockeyPlayer o) {
		return (int)(this.getNumberOfGoals() - o.getNumberOfGoals());
	}
	
	
}
