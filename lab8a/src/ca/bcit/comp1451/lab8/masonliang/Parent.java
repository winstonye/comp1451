/**
 * 
 */
package ca.bcit.comp1451.lab8.masonliang;

/**
 * This class defines a parent
 * @author Mason
 *
 */
public class Parent extends Employee implements Comparable<Parent> {
	private int numberOfHoursSpentPerWeekWithKids;
	private static final double OVER_TIME_PAY_RATE = -2.0;
	
	/**
	 * @param numberOfHoursSpentPerWeekWithKids
	 */
	public Parent(String name, int numberOfHoursSpentPerWeekWithKids) {
		super(name);
		setNumberOfHoursSpentPerWeekWithKids(numberOfHoursSpentPerWeekWithKids);
	}

	/**
	 * @return the numberOfHoursSpentPerWeekWithKids
	 */
	public int getNumberOfHoursSpentPerWeekWithKids() {
		return numberOfHoursSpentPerWeekWithKids;
	}

	/**
	 * @param numberOfHoursSpentPerWeekWithKids the numberOfHoursSpentPerWeekWithKids to set
	 */
	public void setNumberOfHoursSpentPerWeekWithKids(int numberOfHoursSpentPerWeekWithKids) {
		this.numberOfHoursSpentPerWeekWithKids = numberOfHoursSpentPerWeekWithKids;
	}

	/**
	 * @return Dress code
	 */
	@Override
	public String getDressCode() {
		return "anything";
	}
	
	/**
	 * @return Is the employee paid salary?
	 */
	@Override
	public boolean isPaidSalary() {
		return false;
	}
	
	/**
	 * @return Does the position require a post secondary position?
	 */
	@Override
	public boolean postSecondaryEducationRequired() {
		return false;
	}
	
	/**
	 * @return Work
	 */
	@Override
	public String getWorkVerb() {
		return "care";
	}
	
	/**
	 * @return Overtime pay rate
	 */
	@Override
	public double getOverTimePayRate() {
		return OVER_TIME_PAY_RATE;
	}
	
	/**
	 * @return false because parents does not get paid
	 */
	@Override
	public boolean getsPaid() {
		return false;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + numberOfHoursSpentPerWeekWithKids;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Parent))
			return false;
		Parent other = (Parent) obj;
		if (numberOfHoursSpentPerWeekWithKids != other.numberOfHoursSpentPerWeekWithKids)
			return false;
		if (numberOfHoursSpentPerWeekWithKids == other.numberOfHoursSpentPerWeekWithKids)
			return true;
		return true;
	}
	
	/**
	 * Override compareTo so that parents who spend the most times with kids are better
	 */
	@Override
	public int compareTo(Parent o) {
		if(this.getNumberOfHoursSpentPerWeekWithKids() < o.getNumberOfHoursSpentPerWeekWithKids()) {
			return -1;
		}else if(this.getNumberOfHoursSpentPerWeekWithKids() > o.getNumberOfHoursSpentPerWeekWithKids()){
			return 1;
		}
		return 0;
	}
}
