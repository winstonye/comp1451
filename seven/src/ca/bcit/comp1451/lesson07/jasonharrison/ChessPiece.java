package ca.bcit.comp1451.lesson07.jasonharrison;

public abstract class ChessPiece {

	private boolean black;
	private int     points;
	
	public abstract void move();
	
	public boolean isBlack() {
		return black;
	}
	
	public int getPointValue() {
		return points;
	}
	
}
