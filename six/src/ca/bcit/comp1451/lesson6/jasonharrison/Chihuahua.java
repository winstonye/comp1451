package ca.bcit.comp1451.lesson6.jasonharrison;

public class Chihuahua extends Dog{

	public Chihuahua(String name, int yearBorn) {
		super(name, yearBorn);
	}
	
	@Override
	public void speak() {
		System.out.println("yap");
	}
	
	@Override
	public void move() {
		System.out.println("carried in purse");
	}
	
	@Override
	public void bark() {
		System.out.println("yap yap yap");
	}
}
