package ca.bcit.comp1451.lesson6.jasonharrison;

public class Dolphin extends Mammal{
	
	@Override
	public void move() {
		System.out.println("swim");
	}
	
	@Override
	public void speak() {
		System.out.println("squeal");
	}	
}
