package ca.bcit.comp1451.lesson6.jasonharrison;

public class Dog extends Mammal {
	
	private String name;
	private int    yearBorn;
	
	public Dog(String name, int yearBorn) {
		this.name = name;
		this.yearBorn = yearBorn;
	}
	

	
	
	
	@Override
	public int hashCode() {
		return 6;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Dog other = (Dog) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (yearBorn != other.yearBorn)
			return false;
		return true;
	}





	@Override
	public void move() {
		System.out.println("run");
	}
	
	@Override
	public void speak() {
		System.out.println("woof");
	}
	
	void bark() {
		System.out.println("bark bark");
	}
	
	@Override
	public String toString() {
		return super.toString() + 
				" my name is " + name +
				" i was born in " + yearBorn;
	}
	

}
