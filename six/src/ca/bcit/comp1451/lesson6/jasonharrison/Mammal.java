package ca.bcit.comp1451.lesson6.jasonharrison;

public class Mammal {
	
	public void move() {
		System.out.println("moving");
	}
	
	public void speak() {
		System.out.println("speaking");
	}
	
	// cannot be overridden
	private void whatever() {
		
	}
	
	@Override
	public String toString() {
		return "my class is " + this.getClass().getSimpleName();
	}
}
